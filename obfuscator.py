# -*- coding: utf-8 -*-

from rapidxml import RapidXml as rxml
import re

class Obfuscator(object):
    def __init__(self, fileds_filename = 'fields'):
        self.fields = set(line.strip() for line in open(fileds_filename))
        
    def _any(self, value):
        t_value = type(value)
        if t_value == str:
            l_value = len(value)
            if l_value < 2:
                return 'XXXXXX' 
            if l_value == 2:
                o_value = value[0]+'XXXXX'
            else:
                o_value = value[0]+'XXXX'+value[l_value-1]
            return o_value
        else:
            return 0

    def _obfuscate_attr(self, attr, o_node):
        o_value = self._obfuscate_value(attr)
        o_attr = rxml().allocate_attribute(
            attr.name, o_value)
        o_node.append_attribute(o_attr)
    

    def _obfuscate_all_attrs(self, node, o_node):
        [self._obfuscate_attr(attr, o_node) \
            for attr in node.attributes]

    def _obfuscate_value(self, element):
        """ 
        if value of node: parent is node name
        if value of attribute: parent is attribute name
        """
        #return element.value

        if element.name in self.fields:
            return self._any(element.value)

        return element.value


    def _obfuscate_child(self, child, o_node):
        o_child = rxml().allocate_node(child.name)
        self.from_node(child, o_child)
        o_child.value = self._obfuscate_value(child)
        o_node.append_node(o_child)

        

    def from_node(self, node, o_node):
        if not node:
            return

        self._obfuscate_all_attrs(node, o_node)

        [self._obfuscate_child(child, o_node) \
            for child in node.children]

        return o_node

    def from_xml(self, xml_data):
        original_node = rxml(xml_data).first_node()
        obfuscated_node = rxml().allocate_node(original_node.name)
        obfuscate(original_node, obfuscated_node)
        return obfuscated_node.unparse()

def main():
    with open('complex.xml') as f:
        original_node = rxml(f.read()).first_node()
        obfuscated_node = rxml().allocate_node(original_node.name)
        
        obs = Obfuscator()
        obs.from_node(original_node, obfuscated_node)

        print original_node
        print obfuscated_node
        print original_node.unparse() == obfuscated_node.unparse()    

if __name__ == '__main__':
    main()


